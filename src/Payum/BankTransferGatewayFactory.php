<?php

declare(strict_types=1);

namespace TP\GatewayPlugin\Payum;

use TP\GatewayPlugin\Payum\Action\StatusAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;
use TP\GatewayPlugin\Payum\Action\Api\ObtainTokenAction;

final class BankTransferGatewayFactory extends GatewayFactory
{
    protected function populateConfig(ArrayObject $config): void
    {
        $config->defaults(
            [
                'payum.factory_name' => 'tp_payment_bank_transfer',
                'payum.factory_title' => 'Total Processing Payment Subscription',

                // Actions
                'payum.action.status' => new StatusAction(),
                
            ]
        );

        if (false === (bool) $config['payum.api']) {
            $config['payum.default_options'] = [
                'environment' => 'test',
                'entity_id' => '',
                'access_token' => '',
            ];
            $config->defaults($config['payum.default_options']);

            $config['payum.required_options'] = ['environment', 'entity_id', 'access_token'];

            $config['payum.api'] = function (ArrayObject $config) {
                $config->validateNotEmpty($config['payum.required_options']);

                $data = [
                    'environment' => $config['environment'],
                    'entity_id' => $config['entity_id'],
                    'access_token' => $config['access_token'],
                ];

                return new SyliusApi($data);
            };
        }

        // $config['payum.api'] = function (ArrayObject $config) {
        //     $data = [
        //         'environment' => $config['environment'],
        //         'entity_id' => $config['entity_id'],
        //         'access_token' => $config['access_token'],
        //     ];

        //     return new SyliusApi($data);
        // };

        $config['payum.paths'] = array_replace([
            'BankTransferGatewayPlugin' => __DIR__.'/../../templates',
        ], $config['payum.paths'] ?: []);

    }
}
