<?php

declare(strict_types=1);

namespace TP\GatewayPlugin\Payum\Action;

use Payum\Core\Action\ActionInterface;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Request\GetHttpRequest;
use Payum\Core\Request\GetStatusInterface;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Sylius\Component\Core\Model\PaymentInterface as SyliusPaymentInterface;

final class StatusAction implements ActionInterface, GatewayAwareInterface
{
    use GatewayAwareTrait;
    
    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        /** @var SyliusPaymentInterface $payment */
        $payment = $request->getFirstModel();
        $details = $payment->getDetails();

        // Get Request
        $getHttpRequest = new GetHttpRequest();
        $this->gateway->execute($getHttpRequest);

        $successPattern = "/^(000\.000\.|000\.100\.1|000\.[36])/";

        //$rejectGroupOnePattern = "/^(000\.400\.[1][0-9][1-9]|000\.400\.2)/";

        if (preg_match($successPattern, $details['status_code'])) {
            $request->markCaptured();
            return;
        } else {
            $request->markFailed();
            return;
        }
    }

    public function supports($request): bool
    {
        return
            $request instanceof GetStatusInterface &&
            $request->getFirstModel() instanceof SyliusPaymentInterface
        ;
    }
}