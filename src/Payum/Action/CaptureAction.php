<?php

declare(strict_types=1);

namespace TP\GatewayPlugin\Payum\Action;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Exception\UnsupportedApiException;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Reply\HttpResponse;
use Sylius\Component\Core\Model\PaymentInterface as SyliusPaymentInterface;
use Payum\Core\Request\Capture;
use Payum\Core\Request\GetHttpRequest;
use Payum\Core\Request\RenderTemplate;
use TP\GatewayPlugin\Payum\SyliusApi;

final class CaptureAction implements ActionInterface, ApiAwareInterface, GatewayAwareInterface
{

    use GatewayAwareTrait;

    /** @var Client */
    private $client;

    /** @var SyliusApi */
    private $api;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        /** @var SyliusPaymentInterface $payment */
        $payment = $request->getModel();        

        // Get Request
        $getHttpRequest = new GetHttpRequest();
        $this->gateway->execute($getHttpRequest);

        // if postback from capture form
        if (isset($getHttpRequest->query['resourcePath'])) {

            $statusCode = $this->getPaymentStatusCode($getHttpRequest->query['resourcePath']);
            
            $payment->setDetails(['status_code' => $statusCode]);

            // Go to status check
            return;
        }

        // Prepare checkout
        $checkout =  $this->prepareCheckout($payment);

        // Render payment form
        $this->gateway->execute($renderTemplate = new RenderTemplate('@TPGatewayPlugin/checkout.html.twig', array(
            'payment' => $payment,
            'checkout_url' => $request->getToken() ? $request->getToken()->getTargetUrl() : null,
            'checkout_id' => $checkout->id,
        )));
        throw new HttpResponse($renderTemplate->getResult());
    }

    private function prepareCheckout($payment)
    {
        $sku = $payment->getOrder()->getItemUnits()->first()->getOrderItem()->getProduct()->getCode();

        $response = null;
        try {
            $response = $this->client->request('POST', $this->api->getEndpoint() . '/v1/checkouts', [
                'form_params' => [
                    'entityId' => $this->api->getEntityId(),
                    'amount' => number_format($payment->getAmount() / 100, 2, '.', ''),
                    'currency' => $payment->getCurrencyCode(),
                    'paymentType' => 'DB',
                    'createRegistration' => 'true',
                    'merchantTransactionId' => 'medusa-' . $payment->getOrder()->getId(),                    
                    'customer.givenName' => $payment->getOrder()->getCustomer()->getFirstName(),
                    'customer.surname' => $payment->getOrder()->getCustomer()->getLastName(), 
                    'customer.mobile' => $payment->getOrder()->getCustomer()->getPhoneNumber(), 
                    'customer.email' => $payment->getOrder()->getCustomer()->getEmail(),
                    'billing.street1' => $payment->getOrder()->getBillingAddress()->getStreet(),
                    'billing.postcode' => $payment->getOrder()->getBillingAddress()->getPostcode(),
                    'billing.city' => $payment->getOrder()->getBillingAddress()->getCity(),
                    'customParameters[SHOPPER_item_sku]' => $sku,
                ],
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->api->getAccessToken(),
                ],
            ]);

        } catch (RequestException $exception) {
            $response = $exception->getResponse()->getBody()->getContents();
            // Throw new exeception?
        }

        $jsonContents = json_decode($response->getBody()->getContents());

        if($response->getStatusCode() != 200 || $jsonContents->result->code !== '000.200.100' ){
            // Checkout not prepared
            // Throw new exception?            
        }

        return $jsonContents;
    }

    private function getPaymentStatusCode($resourcePath) {
        $response = null;
        
        try {
            $response = $this->client->request('GET', $this->api->getEndpoint() . $resourcePath, [
                'query' => [
                    'entityId' => $this->api->getEntityId(),
                ],
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->api->getAccessToken(),
                ],
                //'debug' => true
            ]);

        } catch (RequestException $exception) {
            $response = $exception->getResponse()->getBody()->getContents();
            // Throw new exeception?
            var_dump($response);
            die();
        }

        $jsonContents = json_decode($response->getBody()->getContents());

        if($response->getStatusCode() != 200 || !isset($jsonContents->result->code)){
            // Checkout not prepared
            // Throw new exception?            
        }

        return $jsonContents->result->code;
    }

    public function supports($request): bool
    {
        return
            $request instanceof Capture &&
            $request->getModel() instanceof SyliusPaymentInterface;
    }

    public function setApi($api): void
    {
        if (!$api instanceof SyliusApi) {
            throw new UnsupportedApiException('Not supported. Expected an instance of ' . SyliusApi::class);
        }

        $this->api = $api;
    }
}