<?php

declare(strict_types=1);

namespace TP\GatewayPlugin\Payum;

final class SyliusApi
{
    /** @var string */
    private $environment;

    /** @var string */
    private $entity_id;

    /** @var string */
    private $access_token;

    public function __construct($data)
    {
        $this->environment = $data['environment'];
        $this->entity_id = $data['entity_id'];
        $this->access_token = $data['access_token'];
    }

    public function getEnvironment(): string
    {
        return $this->environment;
    }

    public function getEndpoint(): string
    {
        return $this->environment == 'test' ? 'https://test.oppwa.com' : 'https://oppwa.com';
    }

    public function getEntityId(): string
    {
        return $this->entity_id;
    }

    public function getAccessToken(): string
    {
        return $this->access_token;
    }
}