<?php

declare(strict_types=1);

namespace TP\GatewayPlugin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;

final class BankTransferSubscriptionConfigurationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'environment',
                ChoiceType::class,
                [
                    'choices' => [
                        'Live' => 'live',
                        'Test' => 'test',
                    ],
                    'label' => 'Endpoint Mode',
                ]
            )->add(
                'entity_id',
                TextType::class,
                [
                    'label' => 'Entity ID',
                    'constraints' => [
                        new NotBlank(
                            [
                                'message' => 'The EntityID has not been provided',
                                'groups' => ['sylius'],
                            ]
                        ),
                        new Length(
                            [
                                'min' => 32,
                                'max' => 32,
                                'exactMessage' => 'Invalid EntityID provided',
                                'groups' => ['sylius'],
                            ]
                        ),
                    ],
                ]
            )
            ->add(
                'access_token',
                TextType::class,
                [
                    'label' => 'Access Token',
                    'constraints' => [
                        new NotBlank(
                            [
                                'message' => 'The Access Token has not been provided',
                                'groups' => ['sylius'],
                            ]
                        ),
                        new Regex(
                            [
                                'pattern' => '/[a-zA-Z0-9]{58}[=]{2}/m',
                                'message' => 'Invalid Access token provided',
                                'groups' => ['sylius'],
                            ]
                        ),
                        new Length(
                            [
                                'min' => 60,
                                'max' => 60,
                                'exactMessage' => 'Invalid Access token provided',
                                'groups' => ['sylius'],
                            ]
                        ),
                    ],
                ]
            )
            ->add(
                'total_control_bearer_token',
                TextType::class,
                [
                    'label' => 'Total Control Bearer Token',
                    'constraints' => [
                        new NotBlank(
                            [
                                'message' => 'The Total Control Bearer Token has not been provided',
                                'groups' => ['sylius'],
                            ]
                        )
                    ],
                ]
            )
            ->add(
                'merchant_entity_id',
                TextType::class,
                [
                    'label' => 'Merchant Level Entity ID',
                    'constraints' => [
                        new NotBlank(
                            [
                                'message' => 'The Merchant Level EntityID has not been provided',
                                'groups' => ['sylius'],
                            ]
                        ),
                        new Length(
                            [
                                'min' => 32,
                                'max' => 32,
                                'exactMessage' => 'Invalid Merchant Level EntityID provided',
                                'groups' => ['sylius'],
                            ]
                        ),
                    ],
                ]
            );
    }
}
