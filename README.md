<p align="center">
    <a href="https://totalprocessing.com.com" target="_blank">
        <img src="https://www.totalprocessing.com/img/brand/dark.png" />
    </a>
</p>

<h1 align="center">Total Processing - Sylius gateway plugin</h1>


## Installation and Setup

1. Run `composer require total-processing/sylius-gateway-plugin`.

2. Append to bundles.php:
    ```php
   \TP\GatewayPlugin\TPGatewayPlugin::class => ['all' => true],
    ```

3. Add a new payment method in the Sylius admin panel and select "Total Processing Payment" and enter your Entity ID and Access Token.
